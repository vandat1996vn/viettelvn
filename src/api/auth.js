import axiosClient from './axiosClient';

const authApi = {
  login(data) {
    const url = `/api-token-auth/`;
    return axiosClient.post(url, data);
  }
};

export default authApi;
