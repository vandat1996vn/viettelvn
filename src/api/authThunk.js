import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import ClientApi from './auth';
import axiosClient from './axiosClient';

const initialState = {
  userList: [],

  successEdit: null,
  loading: false,
};
// Actions
const ACTION = {
  GET_ASSIGNED_TO: 'user/getAssignedTo',
};

export const getAssignedTo = createAsyncThunk(ACTION.GET_ASSIGNED_TO, async (body) => {
  return ClientApi.get();
});

// Reducer
const userSlice = createSlice({
  name: 'user',
  initialState: initialState,
  extraReducers: (builder) => {
    builder
      .addCase(getAssignedTo.pending, (state) => {
        state.loading = true;
      })
      .addCase(getAssignedTo.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getAssignedTo.fulfilled, (state, action) => {
        state.loading = false;
        state.userList = action.payload;
      });
  },
});

export const selectAssignedTo = (state) => state.user.userList;

const { reducer: userReducer } = userSlice;
export default userReducer;
