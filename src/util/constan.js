export const narBar = [
  {
    name: "Bảng điều khiển",
    path: "/admin/dashboard",
    icons: <i className="fa fa-cogs" aria-hidden="true"></i>,
    staff: ["USER" , "ALL", "STAFF"]
  },
  {
    name: "Ghi âm cuộc gọi",
    path: "/admin/recordcall",
    icons: <i className="fa fa-microphone" aria-hidden="true"></i>,
    staff: ["ALL", "USER"]
  },
  {
    name: "Thống kê Agent",
    path: "/admin/statisticalagent",
    icons: <i className="fa fa-address-book-o" aria-hidden="true"></i>,
    staff: ["ALL", "USER"]
  },
  {
    name: "Theo dõi online",
    path: "/admin/followonline",
    icons: <i className="fa fa-podcast" aria-hidden="true"></i>,
    staff: ["ALL", "USER"]
  },
  {
    name: "Thống kê card",
    path: "/admin/statisticalcard",
    icons: <i className="fa fa-tasks" aria-hidden="true"></i>,
    staff: ["STAFF" , "USER"]
  },
  {
    name: "Card online",
    path: "/admin/cardonline",
    icons: <i class="fa fa-link" aria-hidden="true"></i>,
    staff: ["STAFF" , "USER"]
  },
  {
    name: "Card ofline",
    path: "/admin/cardofline",
    icons: <i class="fa fa-chain-broken" aria-hidden="true"></i>,
    staff: ["STAFF" , "USER"]
  }
];
