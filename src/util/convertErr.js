export const handleErr = (data) => {
  switch (data) {
    case 'INCORRECT':
      return 'Tài khoản hoặc mật khẩu không chính xác';
      break;
    case 'NOT_ACTIVE':
      return 'Công ty tạm thời ngưng hoạt động';
      break;
    default:
      return '';
  }
};
