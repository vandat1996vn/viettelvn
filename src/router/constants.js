import FollowOnline from '../page/admin/FollowOnline/FollowOnline';
import RecordCall from '../page/admin/RecordCall/RecordCall';
import StatisticalAgent from '../page/admin/StatisticalAgent/StatiscalAgent';
import StatisticalCard from '../page/admin/StatisticalCard/StatisticalCard';
import Dashboard from '../page/admin/Dashboard/Dashboard';
import CardOnline from '../page/admin/CardOnline/CardOnline';
import CardOfline from '../page/admin/CardOfline/CardOfline';

export const router = [
  {
    path: 'dashboard',
    component: <Dashboard />,
    staff: ['USER'],
  },
  {
    path: 'recordcall',
    component: <RecordCall />,
    staff: ['ALL'],
  },
  {
    path: 'statisticalagent',
    component: <StatisticalAgent />,
    staff: ['ALL'],
  },
  {
    path: 'followonline',
    component: <FollowOnline />,
    staff: ['ALL'],
  },
  {
    path: 'statisticalcard',
    component: <StatisticalCard />,
    staff: ['STAFF'],
  },
  {
    path: 'cardonline',
    component: <CardOnline />,
    staff: ['STAFF'],
  }, 
  {
    path: 'cardofline',
    component: <CardOfline />,
    staff: ['STAFF'],
  }
];
