import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

const Footer = () => {
  var CurrentYear = new Date().getFullYear();

  return (
    <footer className='footer'>
        Copyright © <span id='year'>{CurrentYear} </span>
    </footer>
  );
};

export default Footer;
