import React, { useRef, useEffect } from 'react';
const Header = ({ menu }) => {
  const headerRef = useRef(null);

  return (
    <div className='header-container' ref={headerRef}>
      <header className='header navbar' style={{ marginLeft: menu ? '200px' : '60px' }}>
        header
      </header>
    </div>
  );
};

export default Header;
