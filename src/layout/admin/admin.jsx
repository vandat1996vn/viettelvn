import React, { useEffect, useState } from 'react';
import { Outlet } from 'react-router-dom';
import Navbar from '../narbar/narbar';
import Header from './Header';
import Footer from '../Footer/Footer'
import './admin.css';
import { useDispatch } from 'react-redux';
function Admin() {
  const [menu, setMenu] = useState(true);
  const dispatch = useDispatch();
  return (
    <>
      <div className='layout-page'>
        <div className='menu-vertical' style={{ width: menu && '250px' }}>
          <Navbar setMenu={setMenu} menu={menu} />
        </div>
        <div className={`main-content ${menu ? 'has_menu' : ''}`}>
          <div className='layout-page'>
            <Outlet />
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}

export default Admin;
