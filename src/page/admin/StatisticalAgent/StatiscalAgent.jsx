import React, { useEffect, useState } from "react";
import { getArgen } from "../../../features/argen/argenSlide";
import Loading from "../../../components/common/Loading";
import { useDispatch } from "react-redux";
import moment from "moment";

const StatiscalAgent = () => {
  const dispatch = useDispatch();

  const [argenList, setArgenList] = useState();
  const [filter, setFilter] = useState({});
  const [startDate, setStartDate] = useState(moment().toDate());
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    dispatch(getArgen())
      .unwrap()
      .then((res) => {
        setLoading(true);
        setArgenList(res?.data?.results);
      });
  }, []);

  const handelFilter = (e) => {
    setFilter({ ...filter, [e.target.name]: e.target.value });
  };

  const handleSearch = () => {
    setLoading(false);
    dispatch(
      getArgen({
        ...filter
      })
    )
      .unwrap()
      .then((res) => {
        setLoading(true);
        setLoading(true);
        setArgenList(res?.data?.results);
      });
  };

  const titleTable = [
    "Số máy lẻ",
    "Tên",
    "Đã trả lời",
    "Tổng cuộc gọi",
    "Tổng thời gian",
    "Hóa đơn"
  ];

  let listArgen = argenList?.map((item) => {
    return {
      extension: item.extension,
      names: item.name,
      callAnswered: item.callOutAnswer,
      totalCall: item.callOutgoing,
      totaltime: item.duration,
      billsec: item.duration + item.ringing
    };
  });


  const createCSV = (array) => {
    var keys = Object.keys(array[0]); 

    var result = ""; 
    result += keys.join(","); 
    result += "\n"; 

    array.forEach(function (item) {
      keys.forEach(function (key) {
        result += item[key] + ","; 
      });
      result += "\n"; 
    });

    return result;
  };

  const downloadCSV = (array) => {
    let csv = "data:text/csv;charset=utf-8," + createCSV(array);
    let excel = encodeURI(csv); 

    let link = document.createElement("a");
    link.setAttribute("href", excel); 
    link.setAttribute("download", "test.csv"); 
    link.click();
  };

  return (
    <>
      <div className="box-content">
        <div className="top-record__phone">
          <h3 className="tittle-tops">Thống kê từ số máy lẻ</h3>
          <form>
            <div className="groups-filter__alls">
              <div className="groups-input__alls">
                <p className="label-input__alls">Từ ngày:</p>
                <input
                  type="date"
                  name="from"
                  className="control-alls input-alls input-date start-date"
                  defaultValue={new Date().toJSON().slice(0, 10)}
                  onChange={(e) => {
                    handelFilter(e);
                    setStartDate(e.target.value);
                  }}
                />
              </div>
              <div className="groups-input__alls">
                <p className="label-input__alls">Đến ngày:</p>
                <input
                  type="date"
                  name="to"
                  className="control-alls input-alls input-date end-date"
                  defaultValue={new Date().toJSON().slice(0, 10)}
                  min={startDate}
                  onChange={(e) => handelFilter(e)}
                />
              </div>
              <div className="groups-input__alls">
                <input
                  type="text"
                  className="control-alls input-alls"
                  placeholder="Đường dây"
                  name="extension"
                  onChange={(e) => handelFilter(e)}
                />
              </div>
              <button
                type="submit"
                className="btn-blue__alls"
                value="Tìm kiếm"
                onClick={(event) => {
                  event.preventDefault();
                  handleSearch();
                }}
              >
                Tìm kiếm
              </button>
            </div>
          </form>
        </div>
        <p className="export-file__btn" onClick={() => downloadCSV(listArgen)}>
          <i className="fa fa-download" aria-hidden="true"></i> Export
        </p>
        {loading === true ? (
          <div className="list-argen table-list__all">
            <table className="table-alls">
              <tbody>
                <tr>
                  {titleTable?.map((item, index) => (
                    <td key={index}>{item}</td>
                  ))}
                </tr>
                {listArgen?.map((item, index) => (
                  <tr key={index}>
                    <td>
                      <p>{item.extension}</p>
                    </td>
                    <td>
                      <p className="name-call">{item.names}</p>
                    </td>
                    <td>
                      <p>{item.callAnswered}</p>
                    </td>
                    <td>
                      <p>{item.totalCall}</p>
                    </td>
                    <td>
                      <p>{item.totaltime}</p>
                    </td>
                    <td>
                      <p>{item.billsec}</p>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        ) : (
          <Loading />
        )}
      </div>
    </>
  );
};

export default StatiscalAgent;
