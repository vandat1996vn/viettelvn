import React, { useEffect, useState } from "react";
import { getExtOnline } from "../../../features/extensionOnline/extensionOnline";
import Loading from "../../../components/common/Loading";
import { useDispatch } from "react-redux";
import { useParams } from "react-router";

const CardOnline = () => {
  const dispatch = useDispatch();

  const [extOnline, setExtOnline] = useState([]);
  const [listExtOnline, setListExtOnline] = useState([]);
  const [filter, setFilter] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    dispatch(getExtOnline())
      .unwrap()
      .then((res) => {
        setLoading(true);
        setExtOnline(res?.data?.results);
      });
  }, []);

  const handleSearch = () => {
    
    setLoading(false);
    dispatch(
      getExtOnline({
        ...filter
      })
    )
      .unwrap()
      .then((res) => {
        setLoading(true);
        setExtOnline(res?.data?.results);
      });
  };

  const handelFilter = (e) => {
    setFilter({ ...filter, [e.target.name]: e.target.value });
  };

  const filters = extOnline?.filter((o) => o.data.some((v) => v.status === 2));

  return (
    <>
      <div className="box-content">
        <div className="top-record__phone">
          <h3 className="tittle-tops">Thống kê card</h3>
          <form>
            <div className="groups-filter__alls">
              <div className="groups-input__alls">
                <p className="label-input__alls">Ngày:</p>
                <input
                  type="date"
                  name="day"
                  className="control-alls input-alls input-date start-date"
                  defaultValue={new Date().toJSON().slice(0, 10)}
                  onChange={(e) => handelFilter(e)}
                />
              </div>
              <button
                type="submit"
                className="btn-blue__alls"
                value="Tìm kiếm"
                onClick={(event) => {
                  event.preventDefault();
                  handleSearch();
                }}
              >
                Tìm kiếm
              </button>
            </div>
          </form>
        </div>
        {loading === true ? (
          <div className="list-statistical__card">
            <div className="row">
              {filters?.map((item, index) => (
                <div className="col-lg-3" key={index}>
                  <div className="item-statistical__card">
                    <h3 className="title-statistical__card">{item.key}</h3>
                    <div className="list-post__statistical">
                      {item.data?.map((v, i) => (
                        <div className="item-post__statistical" key={i}>
                          {v.status === 2 ? ( <p>{v.port}</p>) : ''}
                          {(v.status === 2 && (
                            <p className="status-post online"></p>
                          )) ||
                            (v.status === 1 && (
                              <p className="status-post offline d-none"></p>
                            )) ||
                            (v.status === 0 && (
                              <p className="status-post pause d-none"></p>
                            ))}
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        ) : (
          <Loading />
        )}
      </div>
    </>
  );
};

export default CardOnline;
