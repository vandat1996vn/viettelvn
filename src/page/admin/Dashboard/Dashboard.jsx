import React, { useState, useEffect } from "react";
import { BiArrowBack } from "react-icons/bi";
import { getDashboard } from "../../../features/dashboard/dashboardSlice";
import { useDispatch } from "react-redux";
import Loading from "../../../components/common/Loading";

const Dashboard = () => {
  const dispatch = useDispatch();
  const [infoDay, setInfoDay] = useState({});
  const [infoMonth, setInfoMonth] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    dispatch(getDashboard())
      .unwrap()
      .then((res) => {
        setLoading(true)
        setInfoDay(res?.data?.day);
        setInfoMonth(res?.data?.month);
      });
  }, []);

  return (
    <>
      <div className="box-content">
        <div className="top-record__phone">
          <h3 className="tittle-tops">Dashboard</h3>
        </div>
        {loading === true ? (
          <div className="content-dashboard">
            <div className="list-info__dashboard">
              <div className="row gutter-10">
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {infoMonth?.incomingCall !== null ? infoMonth?.incomingCall?.toLocaleString("en-US") : ''}
                    </p>
                    <p className="text-item__dashboards">
                      In comming calls this month
                    </p>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {infoMonth?.incomingTime !== null ? infoMonth?.incomingTime?.toLocaleString("en-US") : ''}
                    </p>
                    <p className="text-item__dashboards">
                      In comming minutes this month
                    </p>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {infoMonth?.outgoingCall !== null ? infoMonth?.outgoingCall?.toLocaleString("en-US") : ''}
                    </p>
                    <p className="text-item__dashboards">
                      Out going calls this month
                    </p>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {infoMonth?.outgoingTime !== null ? infoMonth?.outgoingTime?.toLocaleString("en-US") : ''}
                    </p>
                    <p className="text-item__dashboards">
                      Out going minutes this month
                    </p>
                  </div>
                </div>
              </div>
              <div className="row gutter-10">
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {!infoDay?.incomingCall !== null ? infoDay?.incomingCall?.toLocaleString("en-US") : ''}
                    </p>
                    <p className="text-item__dashboards">
                      Incoming calls today
                    </p>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {!infoDay?.incomingTime !== null ? infoDay?.incomingTime?.toLocaleString("en-US") : ''}
                    </p>
                    <p className="text-item__dashboards">
                      Incoming minutes today
                    </p>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {!infoDay?.outgoingCall !== null ? infoDay?.outgoingCall?.toLocaleString("en-US") : ''}
                    </p>
                    <p className="text-item__dashboards">
                      Outgoing calls today
                    </p>
                  </div>
                </div>
                <div className="col-lg-3">
                  <div className="item-info__dashboard">
                    <BiArrowBack className="icons-info__dashboard" />
                    <p className="number-item__dashboard">
                      {!infoDay?.outgoingTime !== null ? infoDay?.outgoingTime?.toLocaleString("en-US") : ''}
                    </p>
                    <p className="text-item__dashboards">
                      Out going minutes today
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <Loading />
        )}
      </div>
    </>
  );
};

export default Dashboard;
