import React, { useEffect, useState, useRef } from "react";
import { getRecord, getDowAudio } from "../../../features/record/recordSlide";
import { useDispatch } from "react-redux";
import moment from "moment";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Pagination } from "../../../components/common/Pagination";
import { usePaginationState } from "../../../hooks/use-pagination";
import { FaPhoneSlash, FaPhone } from "react-icons/fa";
import { useNavigate } from "react-router-dom";
import Loading from "../../../components/common/Loading";

const RecordCall = () => {
  const dispatch = useDispatch();

  const [recordCall, getRecordCall] = useState([]);
  const [filter, setFilter] = useState({});
  const [startDate, setStartDate] = useState(moment().toDate());
  const [phone, setPhone] = useState("");
  const [audio, setAudio] = useState([]);
  const [checkAll, setCheckAll] = useState(false);
  const [loading, setLoading] = useState(false);
  const pagination = usePaginationState();
  const inputTable = document.querySelectorAll(".check-down__record");
  const inputCheckAll = document.getElementById("input-change__all");

  useEffect(() => {
    setLoading(false);
    dispatch(
      getRecord({
        limit: pagination.perPage,
        offset: pagination.perPage * pagination.page - pagination.perPage
      })
    )
      .unwrap()
      .then((res) => {
        setLoading(true);
        getRecordCall(res?.data);

        inputTable.forEach((item) => {
          item.removeAttribute("checked");
        });
        setAudio([]);
        setCheckAll(false);
        inputCheckAll.checked = false;
      });
  }, [pagination]);

  const handelFilter = (e) => {
    setFilter({ ...filter, [e.target.name]: e.target.value });
  };

  const titleTable = [
    "Ngày gọi",
    "Tên NV",
    "Nguồn",
    "Tới",
    "Điều hướng",
    "Trạng thái",
    "Thời lượng",
    "Đổ chuông",
    "Ghi âm"
  ];

  const listRecord = recordCall?.results?.map((item) => {
    return {
      names: item.name,
      source: item.callerid,
      destination: item.destination,
      direction: item.direction,
      status: item.attended,
      durations: item.duration,
      billsec: item.ringing,
      recording: item.uuid,
      date: item.calldate.replace("T", "/")
    };
  });

  let exportReport = recordCall?.results?.map((item) => {
    return {
      names: item.name,
      source: item.callerid,
      destination: item.destination,
      direction: item.direction,
      status: item.attended,
      durations: item.duration,
      billsec: item.ringing,
      recording: `https://cms.siptrunk.vn:1443/records/?action=record&id=${item.uuid}`,
      date: item.calldate.replace("T", "/")
    };
  });

  const handleSearch = () => {
    setLoading(false);
    if (phone) {
      if (/^[1-9]\d*$/.test(phone)) {
      } else {
        toast.error("Số điện thoại phải là số");
      }
    }
    dispatch(
      getRecord(
        {
          ...filter,
          limit: pagination.perPage,
          offset: pagination.perPage * pagination.page - pagination.perPage
        },
        [pagination]
      )
    )
      .unwrap()
      .then((res) => {
        setLoading(true);
        getRecordCall(res?.data);
      });
  };

  const handleChangeAudio = (e, item) => {
    if (e.target.checked === true) {
      setAudio([...audio, item.recording]);
    } else {
      setAudio(audio.filter((it) => it !== item.recording));
    }
  };

  const handleChangeAllAudio = (e) => {
    if (e.target.checked === true) {
      // inputTable.forEach((item) => {
      //   item.setAttribute("checked", "");
      // });
      setCheckAll(true);
      setAudio([]);
    } else {
      // inputTable.forEach((item) => {
      //   item.removeAttribute("checked");
      // });
      setCheckAll(false);
    }
  };

  useEffect(() => {
    if (checkAll === true) {
      inputTable.forEach((item) => {
        item.setAttribute("checked", "");
        item.checked = true;
      });
      setAudio([...listRecord.map((it) => it.recording)]);
    } else {
      inputTable.forEach((item) => {
        item.removeAttribute("checked");
        item.checked = false;
      });
      setAudio([]);
    }
  }, [checkAll]);

  console.log("audio", audio);

  const dowloadRecord = () => {
    if (audio.length > 0) {
      dispatch(
        getDowAudio({
          uuids: `${audio}`
        })
      )
        .unwrap()
        .then(
          // window.location.href = `https://api.siptrunk.vn/api/download-zip/?uuids=${audio}`
          window.open(
            `https://api.siptrunk.vn/api/download-zip/?uuids=${audio}`,
            "_blank"
          )
        );
    } else {
      toast.error("bạn chưa chọn bản ghi âm");
    }
  };

  const createCSV = (array) => {
    var keys = Object.keys(array[0]);

    var result = "";
    result += keys.join(",");
    result += "\n";

    array.forEach(function (item) {
      keys.forEach(function (key) {
        result += item[key] + ",";
      });
      result += "\n";
    });

    return result;
  };

  const downloadCSV = (array) => {
    let csv = "data:text/csv;charset=utf-8," + createCSV(array);
    let excel = encodeURI(csv);

    let link = document.createElement("a");
    link.setAttribute("href", excel);
    link.setAttribute("download", "record.csv");
    link.click();
  };

  return (
    <>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
      <div className="box-content">
        <div className="top-record__phone">
          <h3 className="tittle-tops">Chi tiết cuộc gọi</h3>
          <form>
            <div className="groups-filter__alls">
              <div className="groups-input__alls">
                <p className="label-input__alls">Từ ngày:</p>
                <input
                  type="date"
                  className="control-alls input-alls input-date start-date"
                  defaultValue={new Date().toJSON().slice(0, 10)}
                  name="from_dt"
                  onChange={(e) => {
                    handelFilter(e);
                    setStartDate(e.target.value);
                  }}
                />
              </div>
              <div className="groups-input__alls">
                <p className="label-input__alls">Đến ngày</p>
                <input
                  type="date"
                  className="control-alls input-alls input-date end-date"
                  defaultValue={new Date().toJSON().slice(0, 10)}
                  name="to_dt"
                  min={startDate}
                  onChange={(e) => handelFilter(e)}
                />
              </div>
              <div className="groups-input__alls">
                <input
                  type="text"
                  name="phone"
                  className="control-alls input-alls"
                  placeholder="Số điện thoại"
                  onChange={(e) => {
                    handelFilter(e);
                    setPhone(e.target.value);
                  }}
                />
              </div>
              <button
                type="submit"
                className="btn-blue__alls"
                value="Tìm kiếm"
                onClick={(event) => {
                  event.preventDefault();
                  handleSearch();
                }}
              >
                Tìm kiếm
              </button>
              <button
                className="btn-dowload__all"
                onClick={(event) => {
                  event.preventDefault();
                  dowloadRecord();
                }}
              >
                Tải xuống file ghi âm
              </button>
            </div>
          </form>
        </div>
        <p className="export-file__btn" onClick={() => downloadCSV(exportReport)}>
          <i className="fa fa-download" aria-hidden="true"></i> Export
        </p>
        {loading === true ? (
          <div className="list-record table-list__all">
            <table className="table-alls">
              <tbody>
                <tr>
                  <td>
                    <div className="check-box__alls">
                      <input
                        type="checkbox"
                        className="form-check-input"
                        id="input-change__all"
                        onChange={(e) => handleChangeAllAudio(e)}
                      />
                      <span className="checkmark"> </span>
                    </div>
                  </td>
                  {titleTable?.map((item, index) => (
                    <td key={index}>{item}</td>
                  ))}
                </tr>
                {listRecord?.map((item, index) => (
                  <tr key={index}>
                    <td>
                      <div className="check-box__alls">
                        <input
                          type="checkbox"
                          className="form-check-input check-down__record"
                          onChange={(input) => handleChangeAudio(input, item)}
                        />
                        <span className="checkmark"> </span>
                      </div>
                    </td>
                    <td>
                      <p className="date-call">{item.date}</p>
                    </td>
                    <td>
                      <p className="name-call">{item.names}</p>
                    </td>
                    <td>
                      <p>{item.source}</p>
                    </td>
                    <td>
                      <p>{item.destination}</p>
                    </td>
                    <td>
                      <p>{item.direction}</p>
                    </td>
                    <td>
                      <p className={item.status ? "not-call" : "on-call"}>
                        {item.status !== 0 ? <FaPhone /> : <FaPhoneSlash />}
                      </p>
                    </td>
                    <td>
                      <p>{item.durations}</p>
                    </td>
                    <td>
                      <p>{item.billsec}</p>
                    </td>
                    <td>
                      <audio
                        controls
                        src={`https://cms.siptrunk.vn:1443/records/?action=record&id=${item.recording}`}
                      >
                        <a
                          href={`https://cms.siptrunk.vn:1443/records/?action=record&id=${item.recording}`}
                        >
                          Download audio
                        </a>
                      </audio>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        ) : (
          <Loading />
        )}
        {loading === false ? (
          // "Không có dữ liệu"
          ""
        ) : (
          <Pagination
            currentPage={pagination.page}
            pageSize={pagination.perPage}
            lastPage={Math.min(
              Math.ceil(
                (recordCall.results ? recordCall?.count : 0) /
                  pagination.perPage
              ),
              Math.ceil(
                (recordCall.results ? recordCall?.count : 0) /
                  pagination.perPage
              )
            )}
            onChangePage={pagination.setPage}
            onChangePageSize={pagination.setPerPage}
            onGoToLast={() =>
              pagination.setPage(
                Math.ceil(
                  (recordCall.results ? recordCall?.count : 0) /
                    pagination.perPage
                )
              )
            }
          />
        )}
      </div>
    </>
  );
};

export default RecordCall;
