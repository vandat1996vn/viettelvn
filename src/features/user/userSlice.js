import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axiosClient from '../../api/axiosClient';

const initialState = {
  loading: false,
  profile: {},
};

const ACTION = {
  GET_PROFILE: 'user/getProfile',
};

export const getProfile = createAsyncThunk(ACTION.GET_PROFILE, async (body) => {
  return axiosClient.get('/users/me/', {
    params: body,
  });
});

const userSlice = createSlice({
  name: 'user',
  initialState: initialState,
  reducers: {
  },
  extraReducers: (builder) => {
    builder
      .addCase(getProfile.pending, (state) => {
        state.loading = true;
      })
      .addCase(getProfile.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getProfile.fulfilled, (state, action) => {
        state.loading = false;
        state.success = false;
        state.profile = action.payload;
      })
  },
});

// Selector
export const { getUserList, resetProfile } = userSlice.actions;

export const selectProfile = (state) => state.user.profile;

// Reducer
const { reducer: userReducer } = userSlice;
export default userReducer;
