import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axiosClient from '../../api/axiosClient';

//////initial
const initialState = {
  loading: false,
  listInfomation: {},
};
// Actions
const ACTION = {
  GET_ARGEN: 'api/getRecord',
  
};

export const getArgen = createAsyncThunk(ACTION.GET_ARGEN, async (body) => {
  return axiosClient.get('/report-agent/', { params: body });
});


const argenSlide = createSlice({
  name: 'api',
  initialState: initialState,
  reducers: {
    Infomation: (state, action) => {
      state.listInfomation = action?.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getArgen.pending, (state) => {
        state.loading = true;
      })
      .addCase(getArgen.rejected, (state) => {
        state.loading = false;
      })
      .addCase(getArgen.fulfilled, (state, action) => {
        state.success = false;
        state.ticketList = action.payload;
      })
  },
});

export const { Infomation } = argenSlide.actions;

const { reducer: argenReducer } = argenSlide;
export default argenReducer;
